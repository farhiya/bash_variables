## Bash script for testing arguments and user input

# Argument
# data a function can take in and use in
# $ touch <argument>
# or
#
# $ mv <argument1> <argument2>

# In scripts if you want to use argument they are defined with $# where # is a number
# for example:

echo $1

# interpolation of strings

echo "this is argument 1 $1"

echo "this is argument 2 $2"

echo "this is argument 3 $3"