## Make a new list, with you favorit movies, and print it out using a loop.

for movie in 'shrek' 'hitch' 'harry potter' 'fast and furious' 'rush hour'
do
echo $movie
done

## Make a list of crazy_x_landlords, use a loop to print them out and at then end tell me how many landlord where printes
## Hint: don't just hard code - use a variable to count.

COUNTER=0
for land_lord in 'jack' 'maxine' 'tony' 'the bearded one' 'dani'
do 
echo $land_lord
COUNTER=$(expr $COUNTER + 1)
done
echo $COUNTER


## Make a new list of best restaurants, and print it out using a loop and in this format:
# > 1 - Sushi Samba
# > 2 - Franco Manca's
# > 3 - Anything with truffle

COUNTER=0
for restaurant in 'Sushi Samba' "Franco Manca's" 'Anything with truffle'
do
COUNTER=$(expr $COUNTER + 1)
echo $COUNTER "-" $restaurant
done