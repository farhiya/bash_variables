# Create a bash script that takes in arguments and does the installation 

hostname=$1

# What happend if I dont give argument (hostname)? 
# create a if condition that checks that script has been called with argument
# if it has, set it to hostname, else let the user know to call the script with a ip end the script (exit code 1) 


ssh -o StrickHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ubuntu@$hostname '

sudo apt update

sudo apt install nginx -y
# Create an is condition when you check if nginx already installed. 
# if installed do nothing 
# else install 

sudo systemctl start nginx 
'